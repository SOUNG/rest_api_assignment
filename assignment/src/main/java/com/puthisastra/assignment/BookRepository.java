package com.puthisastra.assignment;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long>{
	Optional<Book> findById(Long id);

	void setTitle(String string, long l);
}
