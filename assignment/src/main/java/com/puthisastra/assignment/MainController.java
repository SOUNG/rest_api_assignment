package com.puthisastra.assignment;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class MainController {
	
	@Autowired
	private BookRepository bookRepository;
	
	@GetMapping("/create")
	@ResponseBody
	public Book createBook() {
		Book book = new Book();
		book.setTitle("Book");
		book.setPages(1);
		
		return bookRepository.save(book);
	}
	
	@GetMapping
	@ResponseBody
	public List<Book> getAllBooks() {
	   return bookRepository.findAll();
	}
	
	@GetMapping("/getBookById")
	@ResponseBody
	public Optional<Book> getBookById() {
		return bookRepository.findById((long) 1);
	}
	
	@GetMapping("update")
	@ResponseBody
	public Optional<Book> updateBookById() {
		bookRepository.setTitle("book updated title", 1);
	    return bookRepository.findById((long) 1);
	}
	  
	@GetMapping("delete")
	@ResponseBody
	public void deleteBookById() {
	    bookRepository.deleteById((long) 1);
	}
}
